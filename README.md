# truelayer-pokespeare

Provide valuable information about Pokémon in Shakespeare's style.

## Usage

### Installation prerequisites

This project requires a stable version of [Rust](https://www.rust-lang.org/tools/install) to be built.

### Building

The project can be built via the Rust toolchain, either in the debug or release
configuration. The artifacts will be stored in the `target` directory.

```
cargo build
cargo build --release
```

### Run locally

The project can be run via `cargo`, either in the debug or release configuration:

```
cargo run
cargo run --release
```

The API will listen on port 9000.

### Docker deployment

A `Docker` configuration is provided for deployment. The port 9000 is exposed.

## Improvements

- To make the application more microservices-friendly, it would be a good idea
  to store the configuration in `.env` files.
- It would also be a good idea to setup a CI pipeline for building artifacts
  as building them within the Docker image build process is bound to be a
  compute-heavy task.