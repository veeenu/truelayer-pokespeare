FROM rust as builder

WORKDIR /usr/src
COPY . .
RUN cargo install --path .

FROM debian:buster-slim
COPY --from=builder /usr/local/cargo/bin/truelayer-pokespeare /usr/local/bin/pokespeare

EXPOSE 9000
CMD ["/usr/local/bin/pokespeare"]
