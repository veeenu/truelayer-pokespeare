use std::convert::Infallible;

use serde::{
  Deserialize,
  Serialize
};
use reqwest::{
  StatusCode,
};
use warp::{
  Filter,
  Reply,
  Rejection,
  filters::BoxedFilter,
  reject::Reject,
};

// === API URIs ===
// In a production environment, these would ideally be parameterized via a
// `.env` file, should the need for that arise, instead of being hardcoded.
// However, the scope of the project doesn't warrant such added complexity;
// here I chose speed of development, readability and zero-overhead over
// feature-thoroughness.

const POKEAPI_URI: &str = "https://pokeapi.co/api/v2";
const FUNTRANSLATIONS_URI: &str = "https://api.funtranslations.com/translate/shakespeare.json";

// === PokeSpeareError ===
// Type definition for our error type. In real life projects it would be much
// more complex but for this project's purposes a newtype over String will
// suffice. The marker `impl Reject` is necessary to convert this struct into
// a `warp::Rejection` via `warp::reject::custom()`.
#[derive(Serialize, Debug)]
struct PokeSpeareError(String);
impl Reject for PokeSpeareError {}

// === PokeSpeareResponse ===
// Type definition for our API's success data.
#[derive(Serialize, Deserialize, Debug)]
struct PokeSpeareResponse {
  name: String,
  description: String,
}

// === PokéSpeare request function ===
// This function asynchronously performs a single request cycle:
// PokéAPI Pokémon -> PokéAPI Pokémon species -> FunTranslations request
// I deemed appropriate not to fragment the function in many sub-functions
// because 1) there is no reusability advantage, as each request is performed
// only here and 2) Rust's block expressions are convenient enough to
// achieve semantic separation between the three requests.

async fn pokespeare(pokemon: String) -> Result<PokeSpeareResponse, PokeSpeareError> {

  // === PokéAPI species URI ===
  // Retrieve the `pokemon-species` endpoint URI from a request to the
  // `/pokemon/{pokemon}` endpoint in PokéAPI.
  let pokeapi_species_uri: String = {
    #[derive(Deserialize)]
    struct Pokemon {
      species: Species,
    }
    
    #[derive(Deserialize)]
    struct Species {
      url: String,
    }

    let pokeapi_pokemon_uri: String = format!("{}/pokemon/{}", POKEAPI_URI, pokemon);
    reqwest::get(&pokeapi_pokemon_uri)
      .await
      .map(|res| {
        if res.status() == StatusCode::OK {
          Ok(res)
        } else {
          Err(PokeSpeareError(
            format!("PokéAPI responded with {}", res.status())
          ))
        }
      })
      .map_err(|e| {
        PokeSpeareError(format!("Request error: {:?}", e))
      })??
      .json::<Pokemon>()
      .await
      .map_err(|e| {
        PokeSpeareError(format!("PokéAPI Pokémon deserialization error: {}", e))
      })?
      .species.url
  };

  // === PokéAPI flavor text ===
  // Retrieve the last viable `flavor_text_entries` element out of the PokéAPI
  // species request. "Last viable" is hereby defined as the last item in the
  // `flavor_text_entries` vector having english language (i.e.
  // language.name == "en"). Further discussion should be had in order to check
  // whether this is an acceptable assumption.
  let pokeapi_flavor_text: String = {
    #[derive(Deserialize)]
    struct PokemonSpecies {
      flavor_text_entries: Vec<FlavorTextEntry>,
    }

    #[derive(Deserialize)]
    struct FlavorTextEntry {
      flavor_text: String,
      language: Language,
    }

    #[derive(Deserialize)]
    struct Language {
      name: String,
    }

    let pokemon_species: PokemonSpecies = reqwest::get(&pokeapi_species_uri)
      .await
      .map(|res| {
        if res.status() == StatusCode::OK {
          Ok(res)
        } else {
          Err(PokeSpeareError(
            format!("PokéAPI responded with {}", res.status())
          ))
        }
      })
      .map_err(|e| {
        PokeSpeareError(format!("Request error: {:?}", e))
      })??
      .json::<PokemonSpecies>()
      .await
      .map_err(|e| {
        PokeSpeareError(format!("PokéAPI species deserialization error: {}", e))
      })?;

    // Filter the `flavor_text_entries` by those having english language, then
    // extract the `flavor_text` property out of the last one. Fails if no
    // available solution which respects the aforementioned rules is found.
    pokemon_species.flavor_text_entries
      .into_iter()
      .filter(|fte| fte.language.name == "en")
      .last()
      .map(|fte| fte.flavor_text)
      .ok_or_else(|| {
        PokeSpeareError(format!("No english flavor texts found for {}", pokemon))
      })?
  };

  // === Text translation ===
  // Call the FunTranslations' Shakespeare API in order to get the translated
  // text. Deserialize the result (if any), pack it into our API data
  // structure and return it.
  let translated_text: String = {
    #[derive(Serialize)]
    struct FunTranslationsBody {
      text: String,
    }

    #[derive(Deserialize)]
    struct FunTranslationsResponse {
      contents: FunTranslationsResponseContents,
    }

    #[derive(Deserialize)]
    struct FunTranslationsResponseContents {
      translated: String,
    }

    let funtranslations_body = FunTranslationsBody {
      text: pokeapi_flavor_text,
    };

    let client = reqwest::Client::new();
    client.post(FUNTRANSLATIONS_URI)
      .json(&funtranslations_body)
      .send()
      .await
      .map(|res| {
        if res.status() == StatusCode::OK {
          Ok(res)
        } else {
          Err(PokeSpeareError(
            format!("FunTranslations responded with {}", res.status())
          ))
        }
      })
      .map_err(|e| {
        PokeSpeareError(format!("Request error: {:?}", e))
      })??
      .json::<FunTranslationsResponse>()
      .await
      .map_err(|e| {
        PokeSpeareError(format!("FunTranslations deserialization error: {}", e))
      })?
      .contents.translated
  };

  Ok(PokeSpeareResponse {
    name: pokemon,
    description: translated_text,
  })
}

// === Handle rejection ===
// Transform a well-typed rejection in a response with error and text, and
// all other rejections in a HTTP 500 error. In a real world situation, the
// error cases would have to be analyzed in order to produce a data structure
// that makes sense for clients; here, non-well-formed requests simply return an
// error with no explanation. This could also be a sensible choice in terms of
// performance overhead, as straight-up rejection won't incur costs of
// serializing data, composing values and whatnot which could have some impact
// at scale.
async fn handle_rejection(err: Rejection) -> Result<impl Reply, Infallible> {
  if let Some(PokeSpeareError(e)) = err.find() {
    let json = warp::reply::json(&e);
    Ok(warp::reply::with_status(
      json,
      warp::http::StatusCode::NOT_FOUND
    ))
  } else {
    Ok(warp::reply::with_status(
      warp::reply::json(&""),
      warp::http::StatusCode::INTERNAL_SERVER_ERROR
    ))
  }
}

// === PokéSpeare filter ===
// Build the Warp filter which will handle the API requests.
// This is extracted in its own function for testing purposes (see below).
fn pokespeare_filter() -> BoxedFilter<(impl Reply,)> {

  // A wrapper that converts the strongly-domain-typed return values from the
  // request operation into Warp concepts (`Reply` and `Rejection`).
  async fn pokespeare_wrap(pokemon: String) -> Result<impl Reply, Rejection> {
    match pokespeare(pokemon).await {
      Ok(res) => Ok(warp::reply::json(&res)),
      Err(err) => Err(warp::reject::custom(err))
    }
  }

  // `/pokemon/{pokemon}`
  warp::path("pokemon")
    .and(warp::path::param())
    .and(warp::path::end())
    .and_then(pokespeare_wrap)
    .recover(handle_rejection)
    .boxed()
}

// === Serve ===
// Public interface. If there were other filters, adding them here via combinators
// would be the way to have different routes.
pub async fn serve() -> () {
  let filter = pokespeare_filter();

  warp::serve(filter)
    .run(([0, 0, 0, 0], 9000))
    .await
}

#[cfg(test)]
mod tests {
  use super::*;

  // === Test: Happy path ===
  // Assert that retrieving Charizard's description returns a HTTP 200 code.
  // WARNING: this test WILL fail if/when the (very low) rate limit for
  // FunTranslations is reached.
  #[tokio::test]
  async fn test_happy_path() {
    let filter = pokespeare_filter();
    let res = warp::test::request()
      .path("/pokemon/charizard")
      .filter(&filter)
      .await
      .unwrap()
      .into_response();

    assert_eq!(res.status(), 200);
  }

  // === Test: Wrong endpoint ===
  // Assert that trying to access a wrong endpoint returns a HTTP 500 code.
  #[tokio::test]
  async fn test_wrong_endpoint() {
    let filter = pokespeare_filter();
    let res = warp::test::request()
      .path("/digimon/black_dragon_kalameet")
      .filter(&filter)
      .await
      .unwrap()
      .into_response();

    assert_eq!(res.status(), 500);
  }

  // === Test: Non-existant Pokémon ===
  // Assert that trying to access a non-existant pokemon for the correct
  // endpoint returns a HTTP 500 code.
  // CAVEAT: will fail when the Dark Souls' DLC will be released by mistake
  // for a Pokémon game.
  #[tokio::test]
  async fn test_nonexistant_pokemon() {
    let filter = pokespeare_filter();
    let res = warp::test::request()
      .path("/pokemon/artorias_of_the_abyss")
      .filter(&filter)
      .await
      .unwrap()
      .into_response();

    assert_eq!(res.status(), 404);
  }

  // === Test: Empty Pokémon ===
  // Assert that trying to access a pokemon with empty name for the correct
  // endpoint returns a HTTP 500 code. It is actually a special case of the
  // "wrong endpoint" test 
  #[tokio::test]
  async fn test_empty_pokemon() {
    let filter = pokespeare_filter();
    let res = warp::test::request()
      .path("/pokemon/")
      .filter(&filter)
      .await
      .unwrap()
      .into_response();

    assert_eq!(res.status(), 500);
  }
}