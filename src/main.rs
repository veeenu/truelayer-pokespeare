// The separation in a `lib` crate and a `main` binary crate has been made in
// order to eventually separate CLI interaction from core functionality.

#[tokio::main]
async fn main() {
  truelayer_pokespeare::serve().await
}
